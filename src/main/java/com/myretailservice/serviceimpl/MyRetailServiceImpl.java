package com.myretailservice.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myretailservice.model.CurrentPrice;
import com.myretailservice.model.CurrentPriceResponse;
import com.myretailservice.model.MyRetailAddProductRequest;
import com.myretailservice.model.MyRetailResponse;
import com.myretailservice.model.MyRetailUpdateRequest;
import com.myretailservice.model.ProductDesc;
import com.myretailservice.model.ProductPrice;
import com.myretailservice.repository.MyRetailProductDescRepo;
import com.myretailservice.repository.MyRetailProductPriceRepo;
import com.myretailservice.service.MyRetailService;

@Service
public class MyRetailServiceImpl implements MyRetailService {

	@Autowired
	MyRetailProductPriceRepo myRetailProductPriceRepo;
	@Autowired
	MyRetailProductDescRepo myRetailProductDescRepo;

	@Override
	public String updatePrice(Integer productId, MyRetailUpdateRequest requestbody) {
		String status = "Product Price Cannot be Updated";
		try {
			ProductPrice productPrice = myRetailProductPriceRepo.findByProductId(productId);
			if (productPrice != null) {
				productPrice.getCurrentPrice().setValue(requestbody.getValue());
				myRetailProductPriceRepo.save(productPrice);
				status = "Product Price Updated.";
			} else {
				status = "No Such product present.";
			}
		} catch (Exception e) {
			status += " :: Exception :: " + e;

		}
		return status;
	}

	@Override
	public ProductPrice getProduct(Integer productId) {
		return myRetailProductPriceRepo.findByProductId(productId);
	}

	@Override
	public MyRetailResponse getProductAll(Integer productId) {
		MyRetailResponse response = new MyRetailResponse();
		try {
			ProductPrice findByProductId = myRetailProductPriceRepo.findByProductId(productId);
			ProductDesc findByProductId2 = myRetailProductDescRepo.findByProductId(productId);
			response.setProductId(findByProductId.getProductId());
			CurrentPriceResponse priceResponse = new CurrentPriceResponse();
			priceResponse.setValue(findByProductId.getCurrentPrice().getValue());
			priceResponse.setCurrencyCode(findByProductId.getCurrentPrice().getCurrencyCode());
			priceResponse.setProductDesc(findByProductId2.getProductDesc());
			response.setCurrentPrice(priceResponse);

		} catch (Exception e) {

		}
		return response;
	}

	@Override
	public String addProduct(MyRetailAddProductRequest requestbody) {
		ProductPrice productPrice = new ProductPrice();
		ProductDesc productDesc = new ProductDesc();
		String status = "Data not saved";
		try {
			if (requestbody.getProductId() != null) {
				Integer productId = requestbody.getProductId();
				int countFindByProductId = myRetailProductPriceRepo.countFindByProductId(productId);

				if (countFindByProductId != 0) {
					productId += 1;
				}
				productPrice.setProductId(productId);
				CurrentPrice currentPrice = new CurrentPrice();
				currentPrice.setCurrencyCode(requestbody.getCurrencyCode());
				currentPrice.setValue(requestbody.getValue());
				productPrice.setCurrentPrice(currentPrice);
				productDesc.setProductId(productId);
				productDesc.setProductDesc(requestbody.getProductDesc());
				myRetailProductPriceRepo.save(productPrice);
				myRetailProductDescRepo.save(productDesc);
				status = "Data Saved Successfully with id : " + productId;
			}
		} catch (Exception e) {
			status = " :: Exception :: " + e;
		}
		return status;
	}

}
