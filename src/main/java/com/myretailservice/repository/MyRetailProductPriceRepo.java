package com.myretailservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.myretailservice.model.ProductPrice;

@Repository
public interface MyRetailProductPriceRepo extends MongoRepository<ProductPrice, Integer> {

	@Query("{'productId' : ?0}")
	ProductPrice findByProductId(Integer productId);

	int countFindByProductId(Integer productId);

}
