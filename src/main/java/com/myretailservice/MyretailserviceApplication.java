package com.myretailservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyretailserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyretailserviceApplication.class, args);
	}

}
