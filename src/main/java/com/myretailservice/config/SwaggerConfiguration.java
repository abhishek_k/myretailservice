package com.myretailservice.config;

import static com.myretailservice.util.MyRetailUtil.INFO_DESC;
import static com.myretailservice.util.MyRetailUtil.INFO_TITLE;
import static com.myretailservice.util.MyRetailUtil.LICENSE;
import static com.myretailservice.util.MyRetailUtil.LICENSE_URL;
import static com.myretailservice.util.MyRetailUtil.PROFILE_EMAIL;
import static com.myretailservice.util.MyRetailUtil.PROFILE_NAME;
import static com.myretailservice.util.MyRetailUtil.PROFILE_URL;
import static com.myretailservice.util.MyRetailUtil.SERVICE_URL;
import static com.myretailservice.util.MyRetailUtil.VERSION;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	private static final Contact DEFAULT_CONTACT = new Contact(PROFILE_NAME, PROFILE_URL, PROFILE_EMAIL);
	private static final ApiInfo DEFAULT_API_INFO = new ApiInfo(INFO_TITLE, INFO_DESC, VERSION, SERVICE_URL,
			DEFAULT_CONTACT, LICENSE, LICENSE_URL, Collections.emptyList());
	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<>(Arrays.asList("application/json"));

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(Predicates.not(PathSelectors.regex("/error.*"))).build().apiInfo(DEFAULT_API_INFO)
				.produces(DEFAULT_PRODUCES_AND_CONSUMES).consumes(DEFAULT_PRODUCES_AND_CONSUMES);
	}
}
