package com.myretailservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myretailservice.model.MyRetailAddProductRequest;
import com.myretailservice.model.MyRetailResponse;
import com.myretailservice.model.MyRetailUpdateRequest;
import com.myretailservice.model.ProductPrice;
import com.myretailservice.service.MyRetailService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/myRetails")
public class MyRetailController {

	@Autowired
	private MyRetailService myRetailService;

	@PutMapping("/products/{id}")
	@ApiOperation(value = "myretail-service", notes = "to update price of a perticular product id passed in url")
	public ResponseEntity<String> updateProductPrice(@PathVariable Integer id,
			@RequestBody MyRetailUpdateRequest requestbody) {
		String status = myRetailService.updatePrice(id, requestbody);
		if (status.contains("Exception"))
			return new ResponseEntity<String>(status, HttpStatus.INTERNAL_SERVER_ERROR);
		else if ("Product Price Updated.".equalsIgnoreCase(status))
			return new ResponseEntity<String>(status, HttpStatus.OK);
		else
			return new ResponseEntity<String>(status, HttpStatus.NO_CONTENT);
	}

	// get
	@GetMapping("/products/{id}")
	@ApiOperation(value = "myretail-service", notes = "to retreive product price details of a perticular product id passed in url")
	public ResponseEntity<ProductPrice> getProduct(@PathVariable Integer id) {
		ProductPrice product = myRetailService.getProduct(id);
		if (product != null)
			return new ResponseEntity<ProductPrice>(product, HttpStatus.OK);
		else
			return new ResponseEntity<ProductPrice>(product, HttpStatus.NO_CONTENT);
	}

	// get
	@GetMapping("/productsDetails/{id}")
	@ApiOperation(value = "myretail-service", notes = "to retreive product price and description of a perticular product id passed in url")
	public ResponseEntity<MyRetailResponse> getProductAll(@PathVariable Integer id) {
		MyRetailResponse response = myRetailService.getProductAll(id);
		if (response.getProductId() != null)
			return new ResponseEntity<MyRetailResponse>(response, HttpStatus.OK);
		else
			return new ResponseEntity<MyRetailResponse>(response, HttpStatus.NO_CONTENT);
	}

	// post
	@PostMapping("/products")
	@ApiOperation(value = "myretail-service", notes = "to add product")
	public ResponseEntity<String> addProduct(@RequestBody MyRetailAddProductRequest requestbody) {
		String status = myRetailService.addProduct(requestbody);
		if (status.startsWith("Data Saved Successfully"))
			return new ResponseEntity<String>(status, HttpStatus.CREATED);
		else
			return new ResponseEntity<String>(status, HttpStatus.INTERNAL_SERVER_ERROR);

	}

}
