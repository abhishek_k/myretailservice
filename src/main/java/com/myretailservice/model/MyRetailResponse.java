package com.myretailservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyRetailResponse {

	private Integer productId;
	private CurrentPriceResponse currentPrice;

}
