package com.myretailservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.myretailservice.model.ProductDesc;

@Repository
public interface MyRetailProductDescRepo extends MongoRepository<ProductDesc, Integer> {

	@Query("{'productId' : ?0}")
	ProductDesc findByProductId(Integer productId);
}
