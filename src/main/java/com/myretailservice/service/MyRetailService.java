package com.myretailservice.service;

import com.myretailservice.model.MyRetailAddProductRequest;
import com.myretailservice.model.MyRetailResponse;
import com.myretailservice.model.MyRetailUpdateRequest;
import com.myretailservice.model.ProductPrice;

public interface MyRetailService {

	String updatePrice(Integer id, MyRetailUpdateRequest requestbody);

	String addProduct(MyRetailAddProductRequest requestbody);

	ProductPrice getProduct(Integer id);

	MyRetailResponse getProductAll(Integer id);

}
