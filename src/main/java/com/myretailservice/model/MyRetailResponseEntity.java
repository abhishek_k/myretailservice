package com.myretailservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyRetailResponseEntity {

	private MyRetailResponse productDetails;
	private ProductPrice productPrice;
	private String msg;

}
