package com.myretailservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrentPriceResponse {

	private Double value;
	private String currencyCode;
	private String productDesc;

}
