package com.myretailservice.util;

public class MyRetailUtil {

	public static final String INFO_TITLE = "myretail-service";
	public static final String INFO_DESC = "This APIs will create update and return details of product.";
	public static final String VERSION = "1.0";
	public static final String SERVICE_URL = "urn:tos";
	public static final String LICENSE = "InvTrans 1.0";
	public static final String LICENSE_URL = "http://www.apache.org/licenses/LICENSE-2.0";
	public static final String PROFILE_NAME = "Abhishek Karmakar";
	public static final String PROFILE_URL = "";
	public static final String PROFILE_EMAIL = "avi11tech@gmail.com";

}
